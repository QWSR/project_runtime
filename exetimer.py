#commands file is a text file that contains command, seperated by newline characters, to be executed
#basically, create the commands file by writing one commands on each line
#leading spaces and empty lines are ignored

#commands file command example

#./quicksort --stable < size-100.txt > sorted-100.txt
#./quicksort --stable < size-10000.txt > sorted-10000.txt
#./quicksort --stable < size-1000000.txt > sorted-1000000.txt
#./quicksort --stable < size-100000000.txt > sorted-100000000.txt

#test file lists is a text file that contains directories of test input files, seperated by newline 
#   characters
#just like commands file, create the test file list by writing one directory/filename on each line

#test file list example
#test-1-invalid_input.txt
#C:\Users\standardUser\Desktop\test_files\test-2-tricky.txt
#C:\Users\standardUser\Documents\test-3-wrong_direction.txt

import os #os.system()
import time #time.time()

class program_timer:
    def __init__(self):
        test_files=[]
        command_list=[]
        timer_data=[]
        csv_data=""

    #[required:] 'command' be a valid shell command
    #[modifies:] none, directly
    #[effect:] runs command denoted in 'command'
    def run_command(self, command):
        os.system(command)

    #[required:] test_list_file be non-empty and accessible, open_flag be a valid flag, command_list be 
    #   non-empty
    #[modifies:] self.test_files
    #[effect:] reads test files for test input file filenames, stores them in self.test_files, 
    #   and appends every command in command_list with test files

    #NOTE: this functin appends every possible combinations of test files and commands together, for 
    #   any other configurations, just add the inclusion of test files in command.txt 
    def include_test_files(self, test_list_file, open_flag='r'):
        test_file_list=open(test_list_file, open_flag)
        self.test_files=test_file_list.read().split('\n')
        test_file_list.close()

        new_command_list=[]

        for command in self.command_list:
            for test_file_name in self.test_files:
                new_command_list.append(command + ' < ' + test_file_name)

    #[required:] command_list contains valid shell command
    #[modifies:] self.timer_data
    #[effect:] runs and records execution time for all commands in command_list
    def time_all_command(self):
        self.timer_data=[]

        for command in self.command_list:
            lead_free_command=str.lstrip(command)

            if(len(lead_free_command)>0):
                timer_start=time.time()
                self.run_command(lead_free_command)
                timer_end=time.time()
                
                self.timer_data.append([lead_free_command, timer_end-timer_start])

    #[required:] timer_data contains commands and execution times
    #[modifies:] self.csv_data
    #[effect:] converts timer_data into csv form
    def export_to_csv(self):
        self.csv_data=""

        for data_point in self.timer_data:
            self.csv_data += (str.lstrip(data_point[0]) + ', ' + str(data_point[1]) + '\n')
