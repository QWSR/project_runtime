#commands file is a text file that contains command, seperated by newline characters, to be executed
#basically, create the commands file by writing one commands on each line
#leading spaces and empty lines are ignored

#commands file command example

#./quicksort --stable < size-100.txt > sorted-100.txt
#./quicksort --stable < size-10000.txt > sorted-10000.txt
#./quicksort --stable < size-1000000.txt > sorted-1000000.txt
#./quicksort --stable < size-100000000.txt > sorted-100000000.txt

import sys
import exetimer
#[required:] the directory of the commands files must be valid and readable
#the commands in the commands files must be guaranteed to halt
#the program must have the permissions to create and write to files in the specified output directory

#[modifies:] output_command_file and its directory

#[effect:] measures runtime of commands denoted in commands file and saves it in a specified output file

#[usage:]
#user@localhost$ python project_runtime.py
#Please enter the command sheet directory: command.txt
#Save the output file as: quicksort.csv

def main():
    input_command_file=open(input("Please enter the command sheet directory: "), 'r')
   
    program_time_data=exetimer.program_timer()

    program_time_data.command_list=input_command_file.read().split('\n')
    
    input_command_file.close()

    #output file are in the form csv
    output_command_file=open(input("Save the output file as: "), 'a') 
    #output file is opened in 'append' mode
    #so make sure that unwanted data are erased from the output file or the old output file deleted 
    #if the output file exists

    program_time_data.time_all_command()

    program_time_data.export_to_csv()

    output_command_file.write(program_time_data.csv_data)
    output_command_file.close()

if __name__ == "__main__":
    main()
